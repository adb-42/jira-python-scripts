import yaml
import csv

def sync_csv_and_yaml(dir=None,csv_file="ALIMExportedPBS.csv", yaml_file="ALIMExportedPBS.yaml"):
    # Import all data from single data source
    # filename = "data/ALIMExportedPBS.csv"

    filename = dir+csv_file
    all_components = []
    with open(filename, "r") as data:
        for line in csv.DictReader(data):
            # description (str, optional): Component description. Defaults to None.
            # lead_username (str, optional): Username of Component lead. Defaults to None.
            # assignee_type (str, optional): Assignee Type default. Defaults to None.
            data = {
                "name":line["Abbreviated Name"].replace('SOFTWARE','Software'),
                "description":line["Web URL"],
                "lead_username":line["Design Authority"],
                "id":line["ID"],
            }
            all_components.append(data)

    filename=dir+yaml_file
    with open(filename, "w") as data:
        yaml.dump(all_components,data, sort_keys=False, default_flow_style=False)


if __name__ == "__main__":
    sync_csv_and_yaml(dir="data/")

